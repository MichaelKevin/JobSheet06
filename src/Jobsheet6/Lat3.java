package Jobsheet6;
import java.io.Console;
public class Lat3 {
    public static void main(String[] args) {
        String nama;
        int usia;
        String alamat;
        String umur;
        //membuat objek console
        Console con = System.console();
        //mengisi variabel dengan console
        System.out.print("Masukkan nama : ");
        nama = con.readLine();
        System.out.print("Masukkan usia : ");
        umur = con.readLine();
        usia = Integer.parseInt(umur);
        System.out.print("Masukkan alamat : ");
        alamat = con.readLine();
        //output
        System.out.println("Nama kamu adalah " + nama);
        System.out.println("Alamat kamu di " + alamat);
        System.out.println("Usia kamu " + usia + " tahun");
        
    }
    
}
