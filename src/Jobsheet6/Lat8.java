package Jobsheet6;
import java.io.BufferedReader;
import java.io.InputStreamReader;
public class Lat8 {
    public static void main(String[] args) {
        BufferedReader data = new BufferedReader (new InputStreamReader(System.in));
            String x = " ";
            int hitungLuas = 0;
            System.out.println("---Luas Persegi---");
            System.out.print("Masukan sisi : ");
            
            try {
                x = data.readLine();
                int angka = Integer.parseInt(x);
                hitungLuas = angka * angka;
                System.out.println("Luas persegi dengan sisi " + angka + " adalah " + hitungLuas);
            }catch (Exception e){
                System.out.println("Error");
                
            }

    }
    
}
