package Jobsheet6;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Lat2 {

    public static void main(String[] args) throws IOException { 
        String nama;
        //membuat objek inputstream
        InputStreamReader isr = new InputStreamReader(System.in);
        
        //membuat objek buffereader
        BufferedReader br = new BufferedReader(isr);
        
        //mengisi variabel nama dengan BufferedReader
        System.out.print("Inputkan Nama: ");
        nama = br.readLine();
        
        //tampilkan output variabel nama
        System.out.println("Nama kamu adalah " + nama);
        
       
    }
    
}
